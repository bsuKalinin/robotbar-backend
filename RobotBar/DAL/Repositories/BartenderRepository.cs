﻿using DAL.Interfaces;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DAL.Models;

namespace DAL.Repositories
{
	public class BartenderRepository : IBartenderRepository
	{
		private readonly IRobotBarContext _robotBarContext;

		public BartenderRepository(IRobotBarContext robotBarContext)
		{
			_robotBarContext = robotBarContext;
		}

		public async Task<IEnumerable<User>> GetAllBartenders()
		{
			return await _robotBarContext.User.Where(x => x.Role.Name == "bartender").ToListAsync();
		}
	}
}
