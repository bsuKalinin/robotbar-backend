﻿using DAL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Contexts;

namespace DAL.Repositories
{
	public class OrderRepository : IOrderRepository
	{

		private readonly IRobotBarContext _robotBarContext;

		public OrderRepository(IRobotBarContext robotBarContext)
		{
			_robotBarContext = robotBarContext;
		}

		public async Task<IEnumerable<Order>> GetAllOrders()
		{
			return await _robotBarContext.Order.ToListAsync();
		}

		public async Task<IEnumerable<Order>> GetOrderByUserId(int id)
		{
			return await _robotBarContext.Order.Where(x => x.UserId == id).ToListAsync();
		}

		public async Task CreateOrder(Order order)
		{

			await _robotBarContext.Order.AddAsync(order);
			await SaveChanges();
		}

		public async Task FinishOrder(IEnumerable<Order> order)
		{
			_robotBarContext.Order.UpdateRange(order);
			await SaveChanges();
		}

		public async Task<IEnumerable<Order>> GetOrderById(string id)
		{
			return await _robotBarContext.Order.Where(x => x.OrderId == id).ToListAsync();
		}

		private async Task SaveChanges()
		{
			 await ((DbContext)_robotBarContext).SaveChangesAsync();
		}

	}
}
