﻿using DAL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
	public class UserRepository : IUserRepository
	{
		private readonly IRobotBarContext _robotBarContext;

		public UserRepository(IRobotBarContext robotBarContext)
		{
			_robotBarContext = robotBarContext;
		}
		public async Task<User> ValidateUser(string login, string hassPass)
		{
			return await _robotBarContext.User.Where(x => x.Login == login & x.PassHash == hassPass).FirstOrDefaultAsync();
		}

		public async Task<User> GetUserByLogin(string login)
		{
			return await _robotBarContext.User.Where(x => x.Login == login).FirstOrDefaultAsync();
		}
	}
}
