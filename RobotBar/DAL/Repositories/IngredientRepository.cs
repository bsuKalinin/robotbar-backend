﻿using DAL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
	public class IngredientRepository : IIngredientRepository
	{
		private readonly IRobotBarContext _robotBarContext;

		public IngredientRepository(IRobotBarContext robotBarContext)
		{
			_robotBarContext = robotBarContext;
		}

		public async Task<IEnumerable<Ingredient>> GetAllIngredients()
		{
			return await _robotBarContext.Ingredient.ToListAsync();
		}

		public async Task<Ingredient> GetIngredientByName(string ingredientName)
		{
			return await _robotBarContext.Ingredient.Where(x => x.Name == ingredientName).FirstOrDefaultAsync();
		}

		public async Task RemoveUsedIngredients(IEnumerable<Ingredient> ingredients)
		{
			_robotBarContext.Ingredient.UpdateRange(ingredients);
			await SaveChanges();
		}

		public async Task UpdateIngredientCountFromStore(Ingredient ingredient)
		{
			_robotBarContext.Ingredient.Update(ingredient);
			await SaveChanges();
		}

		private async Task SaveChanges()
		{
			await ((DbContext)_robotBarContext).SaveChangesAsync();
		}
	}
}
