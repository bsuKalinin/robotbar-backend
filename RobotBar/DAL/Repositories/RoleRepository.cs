﻿using DAL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
	public class RoleRepository : IRoleRepository
	{
		private readonly IRobotBarContext _robotBarContext;

		public RoleRepository(IRobotBarContext robotBarContext)
		{
			_robotBarContext = robotBarContext;
		}

		public async Task<Role> GetRoleById(int roleId)
		{
			return await _robotBarContext.Role.Where(x => x.RoleId == roleId).FirstAsync();
		}
	}
}
