﻿using DAL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
	public class ProductRepository : IProductRepository
	{
		private readonly IRobotBarContext _robotBarContext;

		public ProductRepository(IRobotBarContext robotBarContext)
		{
			_robotBarContext = robotBarContext;
		}

		public async Task<IEnumerable<Product>> GetAllProducts()
		{
			return await _robotBarContext.Product.ToListAsync();
		}

		public async Task<IEnumerable<Ingredient>> GetProductIngredients(string name)
		{
			return await _robotBarContext.ProductIngredient.Where(x => x.Product.Name == name).Select(z => z.Ingredient).ToListAsync();
		}

		public async Task AddProduct(Product product)
		{
			await _robotBarContext.Product.AddAsync(product);
			await SaveChanges();
		}

		public async Task<Product> GetProductByName(string name)
		{
			return await _robotBarContext.Product.Where(x => x.Name == name).FirstOrDefaultAsync();
		}

		public async Task<Product> GetProductById(int id)
		{
			return await _robotBarContext.Product.FindAsync(id);
		}

		private async Task SaveChanges()
		{
			await ((DbContext)_robotBarContext).SaveChangesAsync();
		}
	}
}
