﻿using System.Collections.Generic;

namespace DAL.Models
{
    public partial class Ingredient
    {
        public Ingredient()
        {
            ProductIngredientXref = new HashSet<ProductIngredient>();
        }

        public int IngredientId { get; set; }
        public string Name { get; set; }
        public int BarCount { get; set; }
        public int StoreCount { get; set; }

        public virtual ICollection<ProductIngredient> ProductIngredientXref { get; set; }
    }
}
