﻿namespace DAL.Models
{
	public class Order
	{
		public int ProductId { get; set; }
		public string OrderId { get; set; }
		public int UserId { get; set; }
		public int ProductCount { get; set; }
		public bool OrderStatus { get; set; }

		public virtual Product Product { get; set; }
		public virtual User User { get; set; }

	}
}
