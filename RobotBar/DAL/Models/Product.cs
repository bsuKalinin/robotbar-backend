﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class Product
    {
        public Product()
        {
            ProductIngredientXref = new HashSet<ProductIngredient>();
        }

        public int ProductId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ProductIngredient> ProductIngredientXref { get; set; }
    }
}
