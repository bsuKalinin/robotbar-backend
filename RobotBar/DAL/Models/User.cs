﻿namespace DAL.Models
{
    public partial class User
    {
        public int UserId { get; set; }
        public string Login { get; set; }
        public string PassHash { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public int RoleId { get; set; }

        public virtual Role Role { get; set; }
    }
}
