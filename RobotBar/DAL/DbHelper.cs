﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace DAL
{
	public static class DbHelper
	{
		public static void DetachAllEntries(this DbContext context)
		{
			foreach (var entry in context.ChangeTracker.Entries().ToList())
			{
				context.Entry(entry.Entity).State = EntityState.Detached;
			}
		}
	}
}
