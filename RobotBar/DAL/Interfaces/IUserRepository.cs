﻿using DAL.Models;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
	public interface IUserRepository
	{
		Task<User> ValidateUser(string login, string hassPass);
		Task<User> GetUserByLogin(string login);
	}
}
