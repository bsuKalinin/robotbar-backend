﻿using DAL.Models;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
	public interface IRoleRepository
	{
		Task<Role> GetRoleById(int id);
	}
}
