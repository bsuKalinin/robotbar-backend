﻿using DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
	public interface IIngredientRepository
	{
		Task<IEnumerable<Ingredient>> GetAllIngredients();
		Task<Ingredient> GetIngredientByName(string ingredientName);
		Task UpdateIngredientCountFromStore(Ingredient ingredient);
		Task RemoveUsedIngredients(IEnumerable<Ingredient> ingredients);
	}
}
