﻿using DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
	public interface IBartenderRepository
	{
		Task<IEnumerable<User>> GetAllBartenders();
	}
}
