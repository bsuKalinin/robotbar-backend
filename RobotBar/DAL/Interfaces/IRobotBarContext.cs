﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace DAL.Interfaces
{
	public interface IRobotBarContext
	{
		DbSet<Ingredient> Ingredient { get; set; }
		DbSet<Product> Product { get; set; }
		DbSet<Order> Order { get; set; }
		DbSet<ProductIngredient> ProductIngredient { get; set; }
		DbSet<Role> Role { get; set; }
		DbSet<User> User { get; set; }
	}
}
