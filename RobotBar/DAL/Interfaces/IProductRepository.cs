﻿using DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
	public interface IProductRepository
	{
		Task<IEnumerable<Product>> GetAllProducts();

		Task AddProduct(Product product);

		Task<IEnumerable<Ingredient>> GetProductIngredients(string name);

		Task<Product> GetProductByName(string productName);

		Task<Product> GetProductById(int id);
	}
}
