﻿using DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
	public interface IOrderRepository
	{
		Task<IEnumerable<Order>> GetAllOrders();

		Task CreateOrder(Order order);

		Task<IEnumerable<Order>> GetOrderByUserId(int id);

		Task FinishOrder(IEnumerable<Order> order);

		Task<IEnumerable<Order>> GetOrderById(string id);
	}
}
