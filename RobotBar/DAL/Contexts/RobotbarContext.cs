﻿using DAL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace DAL.Contexts
{
	public partial class RobotbarContext : DbContext, IRobotBarContext
	{
		public RobotbarContext()
		{
		}

		public RobotbarContext(DbContextOptions<RobotbarContext> options)
			: base(options)
		{
		}

		public DbSet<Ingredient> Ingredient { get; set; }
		public DbSet<Product> Product { get; set; }
		public DbSet<ProductIngredient> ProductIngredient { get; set; }
		public DbSet<Role> Role { get; set; }
		public DbSet<User> User { get; set; }
		public DbSet<Order> Order { get; set; }
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			if (!optionsBuilder.IsConfigured)
			{
				//Use there some servises to save ur credentials, for example AWS Secret Manager.
				optionsBuilder.UseSqlServer("");
			}
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.HasAnnotation("ProductVersion", "2.2.1-servicing-10028");

			modelBuilder.Entity<Ingredient>(entity =>
			{

				entity.HasIndex(e => e.Name)
					.HasName("AK_Ingredient_Name")
					.IsUnique();

				entity.Property(e => e.IngredientId).HasColumnName("ingredientId");

				entity.Property(e => e.BarCount).HasColumnName("bartenderCount");

				entity.Property(e => e.Name)
					.IsRequired()
					.HasColumnName("name")
					.HasMaxLength(64)
					.IsUnicode(false);

				entity.Property(e => e.StoreCount).HasColumnName("storekeeperCount");
			});

			modelBuilder.Entity<Product>(entity =>
			{
				entity.HasIndex(e => e.Name)
					.HasName("AK_Product_Name")
					.IsUnique();

				entity.Property(e => e.ProductId).HasColumnName("productId");

				entity.Property(e => e.Name)
					.IsRequired()
					.HasColumnName("name")
					.HasMaxLength(64)
					.IsUnicode(false);
			});

			modelBuilder.Entity<ProductIngredient>(entity =>
			{
				entity.HasKey(e => new { e.ProductId, e.IngredientId })
					.HasName("PK_ProductIngredient_xref_ingredientId_productId");

				entity.ToTable("ProductIngredient_xref");

				entity.Property(e => e.ProductId).HasColumnName("productId");

				entity.Property(e => e.IngredientId).HasColumnName("ingredientId");

				entity.HasOne(d => d.Ingredient)
					.WithMany(p => p.ProductIngredientXref)
					.HasForeignKey(d => d.IngredientId);

				entity.HasOne(d => d.Product)
					.WithMany(p => p.ProductIngredientXref)
					.HasForeignKey(d => d.ProductId);
			});

			modelBuilder.Entity<Role>(entity =>
			{
				entity.HasIndex(e => e.Name)
					.HasName("AK_Role_Name")
					.IsUnique();

				entity.Property(e => e.Name)
					.IsRequired()
					.HasMaxLength(32);
			});

			modelBuilder.Entity<User>(entity =>
			{
				entity.HasIndex(e => e.Login)
					.HasName("AK_User_login")
					.IsUnique();

				entity.Property(e => e.UserId).HasColumnName("userId");

				entity.Property(e => e.FirstName)
					.IsRequired()
					.HasColumnName("firstName")
					.HasMaxLength(32);

				entity.Property(e => e.Login)
					.IsRequired()
					.HasColumnName("login")
					.HasMaxLength(128);

				entity.Property(e => e.PassHash)
					.IsRequired()
					.HasColumnName("passHash")
					.HasMaxLength(128);

				entity.Property(e => e.RoleId).HasColumnName("roleId");

				entity.Property(e => e.SecondName)
					.IsRequired()
					.HasColumnName("secondName")
					.HasMaxLength(32);

				entity.HasOne(d => d.Role)
					.WithMany(p => p.User)
					.HasForeignKey(d => d.RoleId);
			});

			modelBuilder.Entity<Order>(entity =>
			{
				entity.Property(e => e.OrderStatus).HasColumnName("orderStatus");

				entity.Property(e => e.ProductCount).HasColumnName("productCount");

				entity.Property(e => e.OrderId).HasColumnName("orderId");

				entity.Property(e => e.UserId).HasColumnName("userId");

				entity.Property(e => e.ProductId).HasColumnName("productId");

				entity.HasKey(c => new { c.ProductId, c.OrderId });
			});
		}
	}
}
