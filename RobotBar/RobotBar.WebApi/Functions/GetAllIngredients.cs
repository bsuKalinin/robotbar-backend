using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using DAL.Interfaces;
using System.Linq;
using RobotBar.WebApi.DTOs;

namespace RobotBar.WebApi.Functions
{
	public class GetAllIngredients
	{
		private readonly IIngredientRepository _repository;

		public GetAllIngredients(IIngredientRepository repository)
		{
			_repository = repository;
		}
		[FunctionName("GetAllIngredients")]
		public async Task<IEnumerable<IngredientDto>> Run(
			[HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "ingredients")]  HttpRequest httpRequest,
			ILogger log)
		{

			var ingredients = await _repository.GetAllIngredients();

			var result = ingredients.Select(ingredient => new IngredientDto
			{
				Name = ingredient.Name,
				BarCount = ingredient.BarCount,
				StoreCount = ingredient.StoreCount
			}).ToList();

			return result;
		}
	}
}
