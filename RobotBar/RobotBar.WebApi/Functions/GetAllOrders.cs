using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using RobotBar.WebApi.DTOs.Order;

namespace RobotBar.WebApi.Functions
{
    public class GetAllOrders
    {
        private readonly IOrderRepository _repository;
        private readonly IProductRepository _productRepository;

        public GetAllOrders(IOrderRepository repository, IProductRepository productRepository)
        {
            _repository = repository;
            _productRepository = productRepository;
        }
        [FunctionName("GetAllOrders")]
        public async Task<IEnumerable<OrderDto>> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)] HttpRequest req,
            ILogger log)
        {
            var orders = await _repository.GetAllOrders();

            var result = orders.Select(order => new OrderDto()
            {
                OrderId = order.OrderId,
                OrderStatus = order.OrderStatus,
                ProductCount = order.ProductCount,
                UserId = order.UserId,
                ProductName = _productRepository.GetProductById(order.ProductId).Result.Name
            }).ToList();

            return result;
        }
    }
}
