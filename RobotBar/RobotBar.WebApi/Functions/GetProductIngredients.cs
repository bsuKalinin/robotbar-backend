using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using RobotBar.WebApi.DTOs.Product;

namespace RobotBar.WebApi.Functions
{
    public class GetProductIngredients
    {
		private readonly IProductRepository _repository;

		public GetProductIngredients(IProductRepository repository)
		{
			_repository = repository;
		}

		[FunctionName("GetProductIngredients")]
		public async Task<IEnumerable<ProductIngredientDto>> Run(
			[HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "product")] HttpRequest httpRequest,
			ILogger log)
		{
			var name = httpRequest.Query["name"].ToString();

			var productIngredients = await _repository.GetProductIngredients(name);

			var result = productIngredients.Select(ingredient => new ProductIngredientDto
			{
				Name = ingredient.Name
			}).ToList();

			return result;

		}
	}
}
