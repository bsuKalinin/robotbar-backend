using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.Interfaces;
using System;
using Microsoft.AspNetCore.Mvc;
using DAL.Models;

namespace RobotBar.WebApi
{
	public class GetAllProducts
	{
		private readonly IProductRepository _repository;

		public GetAllProducts(IProductRepository repository)
		{
			_repository = repository;
		}

		[FunctionName("GetAllProducts")]
		public async Task<IEnumerable<ProductDto>> Run(
			[HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "products")] HttpRequest httpRequest,
			ILogger log)
		{
			IEnumerable<Product> products = new List<Product>();

			log.LogInformation("entered to get all products");

			try
			{
				log.LogInformation("trying to get info from repository");
				products = await _repository.GetAllProducts();
			}
			catch (Exception e)
			{
				log.LogError(e.Message);
			}

			var result = products.Select(product => new ProductDto
			{
				Name = product.Name
			}).ToList();

			return result;

		}
	}
}
