using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using DAL.Interfaces;
using System.Linq;
using RobotBar.WebApi.DTOs.Bartender;

namespace RobotBar.WebApi.Functions
{
	public class GetAllBartenders
	{
		private readonly IBartenderRepository _repository;

		public GetAllBartenders(IBartenderRepository repository)
		{
			_repository = repository;
		}

		[FunctionName("GetAllBartenders")]
		public async Task<IEnumerable<BartenderDto>> Run(
			[HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "bartenders")]  HttpRequest httpRequest)
		{

			var bartenders = await _repository.GetAllBartenders();

			var result = bartenders.Select(bartender => new BartenderDto
			{
				Firstname = bartender.FirstName,
				Secondname = bartender.SecondName
			}).ToList();

			return result;
		}
	}
}
