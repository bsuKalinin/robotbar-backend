using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using DAL.Interfaces;
using RobotBar.WebApi.DTOs.Order;
using System.Collections.Generic;
using System.Linq;

namespace RobotBar.WebApi.Functions
{
	public class GetOrdersById
	{
		private readonly IOrderRepository _orderRepository;
		private readonly IUserRepository _userRepository;
		private readonly IProductRepository _productRepository;

		public GetOrdersById(IOrderRepository repository, IUserRepository userRepository, IProductRepository productRepository)
		{
			_orderRepository = repository;
			_userRepository = userRepository;
			_productRepository = productRepository;
		}

		[FunctionName("GetOrdersById")]
		public async Task<IEnumerable<OrderDto>> Run(
			[HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "orders/{loginUser}")] HttpRequest req,
			ILogger log, string loginUser)
		{
			var id =  _userRepository.GetUserByLogin(loginUser).Result.UserId;
			var orders =  _orderRepository.GetOrderByUserId(id).Result;

			var result = orders.Select(order => new OrderDto()
			{
				OrderId = order.OrderId,
				OrderStatus = order.OrderStatus,
				ProductCount = order.ProductCount,
				UserId = order.UserId,
				ProductName = _productRepository.GetProductById(order.ProductId).Result.Name
			}).ToList();

			return result;

		}
	}
}
