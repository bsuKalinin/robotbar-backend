﻿namespace RobotBar.WebApi.DTOs.Order
{
	public class OrderDto
	{
		public string OrderId { get; set; }
		public int UserId { get; set; }
		public int ProductCount { get; set; }
		public bool OrderStatus { get; set; }
		public string ProductName { get; set; }
	}

}
