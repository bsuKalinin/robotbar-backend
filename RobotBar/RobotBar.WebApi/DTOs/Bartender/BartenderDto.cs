﻿namespace RobotBar.WebApi.DTOs.Bartender
{
	public class BartenderDto
	{
        public string Firstname { get; set; }
        public string Secondname { get; set; }
    }
}
