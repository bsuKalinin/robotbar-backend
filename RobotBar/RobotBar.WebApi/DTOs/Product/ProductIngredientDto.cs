﻿namespace RobotBar.WebApi.DTOs.Product
{
	public class ProductIngredientDto
	{
		public string Name { get; set; }
	}
}
