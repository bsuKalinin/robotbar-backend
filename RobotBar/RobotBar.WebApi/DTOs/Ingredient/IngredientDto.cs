﻿namespace RobotBar.WebApi.DTOs
{
	public class IngredientDto
	{
		public int BarCount { get; set; }
		public int StoreCount { get; set; }
		public string Name { get; set; }
	}
}
