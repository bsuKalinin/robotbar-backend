﻿namespace RobotBar.WebApi.DTOs
{
	public class UserDto
	{
		public string Role { get; set; }
		public string FirstName { get; set; }
		public string SecondName { get; set; }
	}
}
