﻿namespace RobotBar.WebApi.Write.DTOs
{
	public class UpdateIngredientsDto
	{
		public string IngredientName { get; set; }
		public int IngredientCount { get; set; }
	}
}
