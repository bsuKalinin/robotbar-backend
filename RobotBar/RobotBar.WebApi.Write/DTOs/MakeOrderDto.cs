using System;
using System.IO;

namespace RobotBar.WebApi.Write.DTOs
{
    public class MakeOrderDto
    {
        public string ProductName { get; set; }
        public int ProductCount { get; set; }
        public string UserLogin { get; set; }
        public string OrderId { get; set; }
    }
}
