using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using DAL.Interfaces;
using System.Collections.Generic;
using DAL.Models;

namespace RobotBar.WebApi.Write
{
	public class FinishOrder
	{
		private readonly IOrderRepository _orderRepository;
		private readonly IIngredientRepository _ingredientRepository;
		private readonly IProductRepository _productRepository;

		public FinishOrder(IOrderRepository orderRepository, IIngredientRepository ingredientRepository, IProductRepository productRepository)
		{
			_orderRepository = orderRepository;
			_ingredientRepository = ingredientRepository;
			_productRepository = productRepository;
		}

		[FunctionName("FinishOrder")]
		public async Task<IActionResult> Run(
			[HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "order/{orderId}")] HttpRequest req,
			ILogger log, string orderId)
		{
			IEnumerable<Ingredient> ingredients = new List<Ingredient>();

			var orders = _orderRepository.GetOrderById(orderId).Result;

			foreach (var order in orders)
			{
				order.OrderStatus = true;
				var productName = _productRepository.GetProductById(order.ProductId).Result.Name;
				ingredients = _productRepository.GetProductIngredients(productName).Result;

				foreach (var ingredient in ingredients)
				{
					if (ingredient.BarCount - order.ProductCount < 0)
					{
						return new BadRequestResult();
					}

					ingredient.BarCount -= order.ProductCount;
				}
			}

			await _orderRepository.FinishOrder(orders);
			await _ingredientRepository.RemoveUsedIngredients(ingredients);

			return new OkResult();
		}
	}
}
