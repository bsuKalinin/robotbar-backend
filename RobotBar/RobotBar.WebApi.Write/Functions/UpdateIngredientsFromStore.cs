using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using DAL.Interfaces;
using System.Net.Http;
using RobotBar.WebApi.Write.DTOs;

namespace RobotBar.WebApi.Write.Functions
{
    public class UpdateIngredientsFromStore
    {
        private readonly IIngredientRepository _ingredientRepository;

        public UpdateIngredientsFromStore(IIngredientRepository ingredientRepository)
        {
            _ingredientRepository = ingredientRepository;
        }

        [FunctionName("UpdateIngredientsFromStore")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "updateingredientsfromstore")] HttpRequestMessage req,
            ILogger log)
        {
            var content = await req.Content.ReadAsStringAsync();

            var request = JsonConvert.DeserializeObject<UpdateIngredientsDto>(content);

            var ingredient = _ingredientRepository.GetIngredientByName(request.IngredientName);

            ingredient.Result.BarCount += request.IngredientCount;
            ingredient.Result.StoreCount += request.IngredientCount;

            await _ingredientRepository.UpdateIngredientCountFromStore(ingredient.Result);

            return new OkResult();
        }
    }
}
