using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using DAL.Interfaces;
using DAL.Models;

namespace RobotBar.WebApi.Write
{
    public class CreateProduct
    {
        private readonly IProductRepository _repository;

        public CreateProduct(IProductRepository repository)
        {
            _repository = repository;
        }
        [FunctionName("CreateProduct")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "createProduct")] HttpRequest req,
            ILogger log)
        {
            var name = req.Query["name"].ToString();

            if (name is null)
            {
                return new BadRequestResult();
            }

            await _repository.AddProduct(new Product() {Name= name });

            return new OkResult();
        }
    }
}
