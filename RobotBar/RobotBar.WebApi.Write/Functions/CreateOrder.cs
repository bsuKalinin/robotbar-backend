using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Net.Http;
using RobotBar.WebApi.Write.DTOs;
using System.Collections.Generic;
using System.Linq;
using DAL.Models;
using DAL.Interfaces;

namespace RobotBar.WebApi.Write.Functions
{
	public class CreateOrder
	{
		private readonly IProductRepository _productRepository;
		private readonly IUserRepository _userRepository;
		private readonly IOrderRepository _orderRepository;

		public CreateOrder(IProductRepository productRepository, IUserRepository userRepository, IOrderRepository orderRepository)
		{
			_productRepository = productRepository;
			_userRepository = userRepository;
			_orderRepository = orderRepository;
		}

		[FunctionName("CreateOrder")]
		public async Task<IActionResult> Run(
			[HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "createOrder")] HttpRequestMessage req,
			ILogger log)
		{
			var content = await req.Content.ReadAsStringAsync();

			var request = JsonConvert.DeserializeObject<MakeOrderDto>(content);

			var order = new Order()
			{
				OrderId = request.OrderId,
				ProductCount = request.ProductCount,
				ProductId = _productRepository.GetProductByName(request.ProductName).Result.ProductId,
				UserId = _userRepository.GetUserByLogin(request.UserLogin).Result.UserId,
				OrderStatus = false
			};

			await _orderRepository.CreateOrder(order);

			return new OkResult();
		}
	}
}
